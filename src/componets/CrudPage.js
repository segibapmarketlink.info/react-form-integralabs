import React from "react";
import Form from "./Form/Form";
import DataTable from "./DataTable/DataTable";
import Tab, { Page } from "./Tab";

export default ({
  children,
  activePageName,
  listDisplay,
  formDisplay,
  formPath,
  collection,
  match: {
    params: { id: itemId }
  },
  history
}) => {
  return (
    <Tab activePageName={activePageName}>
      <Page name="list" display="Lista">
        <DataTable
          displayName={listDisplay}
          formPath={formPath}
          collection={collection}
        >
          {/* Columns */}
        </DataTable>
      </Page>
      <Page name="form" display="Formulário">
        <Form
          displayName={formDisplay}
          name={collection}
          itemId={itemId}
          history={history}
          formPath={formPath}
        >
          {/*Fields  */}
        </Form>
      </Page>
    </Tab>
  );
};

export function List() {}
export function Form() {}
