import React from "react";
import { NavLink } from "react-router-dom";

export default ({ items = [] }) => {
  return (
    <div className="ui inverted segment">
      <div className="ui inverted secondary menu">
        {items
          .filter(item => !!item.display)
          .map(({ display, uri }) => (
            <NavLink
              key={uri}
              exact
              to={uri}
              className="item"
              activeClassName="active"
            >
              {display}
            </NavLink>
          ))}
      </div>
    </div>
  );
};
