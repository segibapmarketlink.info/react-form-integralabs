import React from "react";

export default ({
  name,
  type = "text",
  placeholder,
  value,
  handleChange,
  required
}) => (
  <input
    name={name}
    type={type}
    placeholder={placeholder}
    value={value || ""}
    onChange={handleChange}
    required={required}
  />
);
