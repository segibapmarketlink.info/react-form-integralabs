import React, { useState, useEffect } from "react";
import restService from "../../api/RestService";

export default ({
  children,
  defaultValue = {},
  name,
  displayName,
  confirmDisplay = "Salvar",
  confirmIcon = "cloud upload icon",
  itemId,
  history,
  formPath
}) => {
  const [formData, setFormData] = useState(defaultValue);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const fetchFormData = async () => {
      setLoading(true);
      const json = await restService(`${name}/${itemId}`);
      setFormData({ ...json });
      setLoading(false);
    };
    if (itemId) {
      fetchFormData();
    }
  }, [itemId, name]);

  const fieldChangeHandle = ({ target: { name, value } }) => {
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = async event => {
    event.preventDefault();
    setLoading(true);
    if (formData._id) {
      await restService(`${name}/${itemId}`, "PUT", formData);
    } else {
      await restService(name, "POST", formData);
    }
    setFormData({ ...defaultValue });
    setLoading(false);
    history.push(formPath);
  };

  return (
    <form className="ui form" onSubmit={handleSubmit}>
      {displayName && <h4 className="ui dividing header">{displayName}</h4>}

      {React.Children.map(children, ({ type: Component, props }) => (
        <Component
          {...props}
          key={props.name}
          value={formData[props.name]}
          handleChange={fieldChangeHandle}
        />
      ))}

      <div className="ui message">
        <p>{JSON.stringify(formData, null, 2)}</p>
      </div>
      <div className="ui inverted divider" />
      <button
        className="ui blue huge button"
        type="submit"
        title="Clique para enviar os dados"
      >
        <i className={confirmIcon} />
        {loading ? "Enviando..." : confirmDisplay}
      </button>
      {loading && (
        <div className="ui active dimmer">
          <div className="ui text loader">Enviando os dados...</div>
        </div>
      )}
    </form>
  );
};
