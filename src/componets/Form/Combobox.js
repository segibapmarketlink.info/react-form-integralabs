import React from "react";

export default ({
  name,
  placeholder,
  value,
  items = "",
  handleChange,
  required
}) => (
  <select
    name={name}
    className="ui selection dropdown"
    required={required}
    onChange={handleChange}
    value={value}
  >
    <option>{placeholder}</option>
    {items.split("|").map(item => {
      const [key, label] = item.split("-");
      return (
        <option key={key} value={key}>
          {label}
        </option>
      );
    })}
  </select>
);
