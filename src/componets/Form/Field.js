import React from "react";
import TextField from "./TextField";
import Combobox from "./Combobox";

export default ({
  name,
  type = "text",
  displayName,
  required,

  placeholder,
  value,
  items,
  handleChange
}) => (
  <div className="field">
    <label htmlFor={name}>
      {displayName} {required && <span>*</span>}:
    </label>
    {type.toLowerCase() === "combobox"
      ? Combobox({ name, placeholder, value, items, handleChange, required })
      : TextField({
          name,
          type,
          placeholder,
          value,
          handleChange,
          required
        })}
  </div>
);
