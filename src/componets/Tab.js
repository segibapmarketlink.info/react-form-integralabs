import React, { useState } from "react";

export default function Tab({ children, activePageName = "list" }) {
  const [activePage, setActivePage] = useState(activePageName);

  let pageContent = null;

  return (
    <>
      <div className="ui pointing menu">
        {React.Children.toArray(children)
          .filter(page => page.type === Page)
          .map(({ props: { name, display, children } }) => {
            name === activePage && (pageContent = children);
            return (
              <a
                key={name}
                className={name === activePage ? "item active" : "item"}
                href="#/"
                onClick={e => setActivePage(name)}
              >
                {display}
              </a>
            );
          })}
      </div>
      <div className="ui segment">{pageContent}</div>
    </>
  );
}
export function Page() {}
