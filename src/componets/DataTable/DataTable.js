import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import restService from "../../api/RestService";

export default ({ collection, children, formPath, displayName }) => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const fetchDataItem = async () => {
      setLoading(true);
      const dataItems = await restService(collection);
      setItems([...dataItems]);
      setLoading(false);
    };
    fetchDataItem();
  }, [collection]);

  const removeItem = async itemId => {
    setLoading(true);
    await restService(`${collection}/${itemId}`, "DELETE");
    setItems(items.filter(item => item._id.$oid !== itemId));
    setLoading(false);
  };

  return (
    <div className="ui segment">
      {displayName && <h4 className="ui dividing header">{displayName}</h4>}
      <table className="ui selectable celled table">
        <thead>
          <tr>
            <th className="center aligned">ID</th>
            {children.map(({ props: { display } }) => (
              <th className="center aligned" key={display}>
                {display}
              </th>
            ))}
            <th />
          </tr>
        </thead>
        <tbody>
          {!items.length ? (
            <tr className="center aligned">
              <td colSpan={children.length + 2}>Não há itens a ser exibidos</td>
            </tr>
          ) : (
            items.map(item => (
              <tr key={item._id.$oid}>
                <td>
                  <NavLink
                    to={`${formPath}/${item._id.$oid}`}
                    title="Clique para alterar o item"
                  >
                    {item._id.$oid}
                  </NavLink>
                </td>

                {children.map(({ props: { value } }) => (
                  <td key={item[value]}>{item[value]}</td>
                ))}

                <td className="center aligned">
                  <button
                    className="ui negative basic button"
                    title="Clique para remover o item"
                    onClick={() => removeItem(item._id.$oid)}
                  >
                    <i className="trash icon" />
                    Remover
                  </button>
                </td>
              </tr>
            ))
          )}
        </tbody>
      </table>
      {loading && (
        <div className="ui active dimmer">
          <div className="ui text loader">Recebendo os dados...</div>
        </div>
      )}
    </div>
  );
};
