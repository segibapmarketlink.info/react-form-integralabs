import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Menu from "./componets/Menu";
import LoginPage from "./pages/LoginPage";
import UserPage from "./pages/UserPage";
import CustomerPage from "./pages/CustomerPage";
import IndexPage from "./pages/IndexPage";
import ProductPage from "./pages/ProductPage";
import MusicPage from "./pages/MusicPage";

const menus = [
  { display: "Home", uri: "/", component: IndexPage },
  { display: "Login", uri: "/login", component: LoginPage },
  { display: "Novo Usuário", uri: "/sing-up", component: UserPage },
  { uri: "/user/:id", component: UserPage },
  { display: "Clientes", uri: "/customer", component: CustomerPage },
  { uri: "/customer/:id", component: CustomerPage },
  { display: "Produtos", uri: "/product", component: ProductPage },
  { uri: "/product/:id", component: ProductPage },
  { display: "Música", uri: "/music", component: MusicPage },
  { uri: "/music/:id", component: MusicPage }
];

export default () => (
  <div className="ui container">
    <h1 className="ui center aligned dividing header">
      React Form - by Robson Leite
    </h1>
    <BrowserRouter>
      <Menu items={menus} />
      <Switch>
        {menus.map(({ display, uri, component: Page, exact }) => (
          <Route
            key={uri}
            exact
            path={uri}
            component={props => (
              <Page {...props} activePageName={display ? "list" : "form"} />
            )}
          />
        ))}
        <Route
          path="*"
          component={() => <h1>A página solicitada não foi encontrada.</h1>}
        />
      </Switch>
    </BrowserRouter>
  </div>
);
