import React from "react";
import Form from "../componets/Form/Form";
import Field from "../componets/Form/Field";

export default () => (
  <Form displayName="Login" confirmDisplay="Login" confirmIcon="user icon">
    <Field
      name="user"
      type="email"
      required
      displayName="Usuário"
      placeholder="username@domain.com"
    />
    <Field
      name="password"
      required
      type="password"
      displayName="Senha"
      placeholder="****"
    />
  </Form>
);
