import React from "react";

export default () => (
  <>
    <h1 className="ui dividing header">Sobre o projeto...</h1>
    <h2 className="ui header">
      <img
        className="ui image"
        src="https://cdn4.iconfinder.com/data/icons/logos-3/600/React.js_logo-512.png"
        alt="react logo"
      />
      <div className="content">React</div>
    </h2>
    <h3 className="first">Motivação</h3>
    <p>
      Esse projeto foi desenvolvido em
      <a
        rel="noopener noreferrer"
        href="https://pt-br.reactjs.org/"
        target="_blank"
      >
        <span> ReactJS </span>
      </a>
      e
      <a
        rel="noopener noreferrer"
        href="https://semantic-ui.com/"
        target="_blank"
      >
        <span> Semantic-UI </span>
      </a>
      por
      <a
        rel="noopener noreferrer"
        href="https://github.com/robsoncarvalholeite"
        target="_blank"
      >
        <span> Robson Leite </span>
      </a>
      como prova de conceito para uma componentização de formulários automáticos
      e performáticos.
    </p>
    <h3 className="first">Repositório</h3>
    <p>
      Caso tenha interesses em ver como foi escrito, pegue o código fonte
      original no repositório temporário no Gitlab
      <a
        rel="noopener noreferrer"
        href="https://gitlab.com/segibapmarketlink.info/react-form-integralabs/"
        target="_blank"
      >
        <span> (clicando aqui)</span>
      </a>
      .
    </p>
  </>
);
