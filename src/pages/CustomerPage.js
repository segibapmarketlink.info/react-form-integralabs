import React from "react";
import Form from "../componets/Form/Form";
import Field from "../componets/Form/Field";
import DataTable from "../componets/DataTable/DataTable";
import DataColumn from "../componets/DataTable/DataColumn";
import Tab, { Page } from "../componets/Tab";

export default ({ activePageName, match: { params }, history }) => (
  <Tab activePageName={activePageName}>
    <Page name="list" display="Lista">
      <DataTable
        displayName="Lista de Clientes"
        formPath="/customer"
        collection="customers"
      >
        <DataColumn display="Nome Completo" value="name" />
        <DataColumn display="Sexo" value="gender" />
        <DataColumn display="Empresa" value="company" />
        <DataColumn display="Email" value="email" />
      </DataTable>
    </Page>
    <Page name="form" display="Formulário">
      <Form
        displayName="Novo Cliente"
        name="customers"
        itemId={params.id}
        history={history}
        formPath="/customer"
      >
        <Field
          name="name"
          displayName="Nome Completo"
          placeholder="José da Silva"
          required
        />
        <Field
          name="gender"
          displayName="Sexo"
          type="combobox"
          items="M-Masculino|F-Feminino|N-São Paulino"
        />
        <Field
          name="company"
          displayName="Empresa"
          placeholder="Empresa do Eduardo"
        />
        <Field
          name="email"
          type="email"
          displayName="Email de Contato"
          placeholder="username@domain.com"
          required
        />
      </Form>
    </Page>
  </Tab>
);
