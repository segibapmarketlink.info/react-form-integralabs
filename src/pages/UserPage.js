import React from "react";
import Form from "../componets/Form/Form";
import Field from "../componets/Form/Field";
import DataTable from "../componets/DataTable/DataTable";
import DataColumn from "../componets/DataTable/DataColumn";
import Tab, { Page } from "../componets/Tab";

export default ({ activePageName, match: { params }, history }) => (
  <Tab activePageName={activePageName}>
    <Page name="list" display="Lista">
      <DataTable
        displayName="Lista de Usuários"
        formPath="/user"
        collection="users"
      >
        <DataColumn display="Usuário" value="user" />
        <DataColumn display="Nível" value="level" />
        <DataColumn display="Nome" value="firstName" />
      </DataTable>
    </Page>
    <Page name="form" display="Formulário">
      <Form
        displayName="Novo Usuário"
        name="users"
        itemId={params.id}
        history={history}
        formPath="/sing-up"
      >
        <Field
          name="firstName"
          required
          displayName="Nome"
          placeholder="José"
        />
        <Field name="lastName" displayName="Sobrenome" placeholder="da Silva" />
        <Field
          name="level"
          type="Combobox"
          required
          displayName="Nível de Acesso"
          items="A-Administrador|G-Gerente Responsável|O-Operador"
        />
        <Field
          name="user"
          type="email"
          required
          displayName="Usuário"
          placeholder="username@domain.com"
        />
        <Field
          name="password"
          required
          type="password"
          displayName="Senha"
          placeholder="****"
        />
        <Field
          name="password2"
          required
          type="password"
          displayName="Repita a senha"
          placeholder="****"
        />
      </Form>
    </Page>
  </Tab>
);
