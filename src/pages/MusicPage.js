import React from "react";
import Form from "../componets/Form/Form";
import Field from "../componets/Form/Field";
import DataTable from "../componets/DataTable/DataTable";
import DataColumn from "../componets/DataTable/DataColumn";
import Tab, { Page } from "../componets/Tab";

export default ({ activePageName, match: { params }, history }) => (
  <Tab activePageName={activePageName}>
    <Page name="list" display="Lista">
      <DataTable
        displayName="Lista de Músicas"
        formPath="/music"
        collection="musics"
      >
        <DataColumn display="Nome" value="name" />
        <DataColumn display="Autor" value="author" />
        <DataColumn display="Estilo" value="style" />
      </DataTable>
    </Page>
    <Page name="form" display="Formulário">
      <Form
        displayName="Formulário de Músicas"
        name="musics"
        itemId={params.id}
        history={history}
        formPath="/music"
      >
        <Field
          name="name"
          displayName="Nome da Música"
          placeholder="Florentina"
          required
        />
        <Field
          name="author"
          displayName="Nome do Autor da Música"
          placeholder="Tiririca"
          required
        />
        <Field
          name="style"
          type="Combobox"
          required
          displayName="Estilo da Música"
          items="pagode-Pagode|rock-Rock|dubstep-Dubstep|gospel-Gospel"
        />
      </Form>
    </Page>
  </Tab>
);
