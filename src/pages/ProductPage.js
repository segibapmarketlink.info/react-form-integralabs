import React from "react";
import Form from "../componets/Form/Form";
import Field from "../componets/Form/Field";
import DataTable from "../componets/DataTable/DataTable";
import DataColumn from "../componets/DataTable/DataColumn";
import Tab, { Page } from "../componets/Tab";

export default ({ activePageName, match: { params }, history }) => (
  <Tab activePageName={activePageName}>
    <Page name="list" display="Lista">
      <DataTable
        displayName="Lista de Produtos"
        formPath="/product"
        collection="products"
      >
        <DataColumn display="Descrição" value="description" />
        <DataColumn display="Preço" value="price" />
      </DataTable>
    </Page>
    <Page name="form" display="Formulário">
      <Form
        displayName="Formulário de Produto"
        name="products"
        itemId={params.id}
        history={history}
        formPath="/product"
      >
        <Field
          name="description"
          displayName="Nome do Produto"
          placeholder="Notebook Dell"
          required
        />
        <Field name="price" displayName="Valor" placeholder="1.500" required />
      </Form>
    </Page>
  </Tab>
);
