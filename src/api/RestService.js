const dataBase = "react-form-integralabs";
const apiKey = "Aps7q9clIquzIVj75x6iSLgJl0ntnAZi";
const apiURL = `https://api.mlab.com/api/1/databases/${dataBase}/collections/`;

export default async (uri, method = "GET", body) => {
  let params = {
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    method
  };

  body && (params.body = JSON.stringify(body));

  const url = apiURL.concat(uri).concat(`/?apiKey=${apiKey}`);
  const response = await fetch(url, params);
  return await response.json();
};
